
$(document).ready(function(){
  var evt = $.browser.msie ? "click" : "change";
  
  if ($.browser.msie) {
    var expand   = function(){ $(this).width(120) }
    var contract = function(){ if (!this.noHide) $(this).width(60) }
    var focus    = function(){ this.noHide = true }
    var blur     = function(){ this.noHide = false; contract.call(this) }
    $("#edit-state")
      .hover(expand, contract)
      .focus(focus)
      .click(focus)
      .blur(blur)
      .change(blur)
  }
  
  // Inline phone number validation
  $("input[name^='phone'][name!='phone_work_ext']").bind(evt, function formatUSPhone(input){
    var phonePattern = /^(\d\d\d)(\d\d\d)(\d\d\d\d)$/;
  	var phonePatternShort = /^(\d\d\d)(\d\d\d\d)$/;
  	var match;
  	
  	var newNum = $(this).val().replace(/[^0-9]/g, '');
  	
  	if ((match = phonePattern.exec(newNum))){
  		newNum = '(' + match[1] + ') ' + match[2] + '-' + match[3];
  	}
  	else if ((match = phonePatternShort.exec(newNum))){
  		newNum = '(   ) ' + match[1] + '-' + match[2];
  	}
  	
  	$(this).val(newNum);
  });
  
  // Set up default states
  if ($("input[name='select_amount']:checked").val() != 'other') {
    $("div.amount").hide();
  }
  //alert(getQuerystring('r'));
  if (($("input[name='recurring_enable']").length > 0) && (getQuerystring('r') != '1')) {
    if ($("input[name='recurring_enable']:checked").val() == 'false') {
      $("div.recurring").hide();
    }
    if ($("#edit-recurring").attr('checked') == false) {
      $("input[name='recurring_select']").attr("disabled","disabled");
    }
    if ($("input[name='recurring_select']:checked").val() != 'date') {
      $("#edit-recurring-month").attr("disabled","disabled");
      $("#edit-recurring-year").attr("disabled","disabled");
    }
  }
  
  $("input[name='select_amount']").bind(evt, function(){
    if ($(this).val() == 'other') {
      $("div.amount").show();
    }
    else {
      $("div.amount").hide();
    }
  });
  $("input[name='recurring_enable']").bind(evt, function(){
    if ($(this).val() == 'true') {
      $("div.recurring").show();
    }
    else {
      $("div.recurring").hide();
    }
  });
  $("#edit-recurring").bind(evt, function(){
    if ($(this).attr('checked')) {
      $("input[name='recurring_select']").removeAttr("disabled");
    }
    else {
      $("input[name='recurring_select']").attr("disabled","disabled");
    }
  });
  $("input[name='recurring_select']").bind(evt, function(){
    if ($(this).val() == 'date') {
      $("#edit-recurring-month").removeAttr("disabled");
      $("#edit-recurring-year").removeAttr("disabled");
    }
    else {
      $("#edit-recurring-month").attr("disabled","disabled");
      $("#edit-recurring-year").attr("disabled","disabled");
    }
  });
});

function popUpWindowHelp(sWindowName, sUrl, iWidth, iHeight) {
	var winLeft = (screen.width - iWidth) / 2;
	var winTop = (screen.height - iHeight - 50) / 2;
	var sWindow = window.open(sUrl, '', 'location=no,status=no,RESIZABLE,width='+iWidth+',height='+iHeight+',toolbar=no,scrollbars=no,left=' + winLeft + ',top=' + winTop);
	sWindow.focus();
	return false;
}

function textareaMaxLength(field, evt, limit) {
  var evt = (evt) ? evt : event;
  var charCode =
    (typeof evt.which != "undefined") ? evt.which :
   ((typeof evt.keyCode != "undefined") ? evt.keyCode : 0);

  if (!(charCode >= 13 && charCode <= 126)) {
    return true;
  }

  return (field.value.length < limit);
}

function getQuerystring(key, default_) {
  if (default_==null) default_=""; 
  key = key.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regex = new RegExp("[\\?&]"+key+"=([^&#]*)");
  var qs = regex.exec(window.location.href);
  if(qs == null)
    return default_;
  else
    return qs[1];
}