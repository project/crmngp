<?php

//save a cwp contribute Transaction
function crmngp_cwp_transaction_save($contribution = NULL, $full_response = FALSE) {
  if (!$contribution) { return NULL; }

  // set some defaults for the SOAP server.
  $contribution['id']     = !empty($contribution['id']) ? $contribution['id'] : 0;
  $contribution['date']   = !empty($contribution['date']) ? $contribution['date'] : date('Y-m-d');
  $contribution['cycle']  = !empty($contribution['cycle']) ? $contribution['cycle'] : date('Y');
  $contribution['batch']  = (!empty($contribution['batch']) && is_numeric($contribution['batch'])) ? $contribution['batch'] : 1;
  $period_values = array('C', 'G', 'H', 'O', 'P', 'R', 'S'); // see function docs for explanation.
  $contribution['period'] = in_array($contribution['period'], $period_values) ? $contribution['period'] : 'G';

  $contribution['phone_home'] = !empty($contribution['phone_home']) ? preg_replace('/\D/','',$contribution['phone_home']) : NULL;
  $contribution['phone_work'] = !empty($contribution['phone_work']) ? preg_replace('/\D/','',$contribution['phone_work']) : NULL;

  $xml  = '<PostVerisignTransaction>';
  $xml .=   '<ContactInfo>';
  $xml .=     !empty($contribution['last_name']) ? '<LastName>'.htmlspecialchars($contribution['last_name']).'</LastName>' : '<LastName />';
  $xml .=     !empty($contribution['first_name']) ? '<FirstName>'.htmlspecialchars($contribution['first_name']).'</FirstName>' : '<FirstName />';
  $xml .=     !empty($contribution['middle_name']) ? '<MiddleName>'.htmlspecialchars($contribution['middle_name']).'</MiddleName>' : '<MiddleName />';
  $xml .=	    '<Prefix /><Suffix/>';
  $xml .=     !empty($contribution['address1']) ? '<Address1>'.htmlspecialchars($contribution['address1']).'</Address1>' : '<Address1 />';
  $xml .=     !empty($contribution['address2']) ? '<Address2>'.htmlspecialchars($contribution['address2']).'</Address2>' : '<Address2 />';
  $xml .=     !empty($contribution['address3']) ? '<Address3>'.htmlspecialchars($contribution['address3']).'</Address3>' : '<Address3 />';
  $xml .=     !empty($contribution['city']) ? '<City>'.htmlspecialchars($contribution['city']).'</City>' : '<City />';
  $xml .=     !empty($contribution['state']) ? '<State>'.htmlspecialchars($contribution['state']).'</State>' : '<State />';
  $xml .=     !empty($contribution['zip']) ? '<Zip>'.htmlspecialchars($contribution['zip']).'</Zip>' : '<Zip />';
  $xml .=     '<Salutation />';
  $xml .=     !empty($contribution['email']) ? '<Email>'.htmlspecialchars($contribution['email']).'</Email>' : '<Email />';
  $xml .=     !empty($contribution['phone_home']) ? '<HomePhone>'.htmlspecialchars($contribution['phone_home']).'</HomePhone>' : '<HomePhone />';
  $xml .=     !empty($contribution['phone_work']) ? '<WorkPhone>'.htmlspecialchars($contribution['phone_work']).'</WorkPhone>' : '<WorkPhone />';
  $xml .=     !empty($contribution['phone_work_ext']) ? '<WorkExtension>'.htmlspecialchars($contribution['phone_work_ext']).'</WorkExtension>' : '<WorkExtension />';
  $xml .=     '<FaxPhone />';
  $xml .=     !empty($contribution['employer']) ? '<Employer>'.htmlspecialchars($contribution['employer']).'</Employer>' : '<Employer />';
  $xml .=     !empty($contribution['occupation']) ? '<Occupation>'.htmlspecialchars($contribution['occupation']).'</Occupation>' : '<Occupation />';
  $xml .=     !empty($contribution['optin']) ? '<OptIn>'.htmlspecialchars($contribution['optin']).'</OptIn>' : '<OptIn />';
  //$xml .=     !empty($contribution['place_of_business']) ? '<placeOfBusiness>'.htmlspecialchars($contribution['place_of_business']).'</placeOfBusiness>' : '<placeOfBusiness />';
  $xml .=     !empty($contribution['contact_type']) ? '<MainType>'.htmlspecialchars($contribution['contact_type']).'</MainType>' : '<MainType />';
  $xml .=     !empty($contribution['organization']) ? '<Organization>'.htmlspecialchars($contribution['organization']).'</Organization>' : '<Organization />';
  $xml .=     !empty($contribution['main_codes']) ? '<MainCodes>' . htmlspecialchars($contribution['main_codes']) . '</MainCodes>' : '';
  $xml .=   '</ContactInfo>';
  $xml .=   '<ContributionInfo>';
  $xml .=     !empty($contribution['cycle']) ? '<Cycle>'.htmlspecialchars($contribution['cycle']).'</Cycle>' : '<Cycle />';
  $xml .=     !empty($contribution['member']) ? '<Member>'.htmlspecialchars($contribution['member']).'</Member>' : '<Member />';
  $xml .=     !empty($contribution['attribution']) ? '<Attribution>'.htmlspecialchars($contribution['attribution']).'</Attribution>' : '<Attribution />';
  $xml .=     !empty($contribution['source']) ? '<Source>'.htmlspecialchars($contribution['source']).'</Source>' : '<Source />';
  $xml .=     !empty($contribution['period']) ? '<Period>'.htmlspecialchars($contribution['period']).'</Period>' : '<Period />';
  if (!empty($contribution['recurring_enable']) && ($contribution['recurring_enable'] == 'true') && !empty($contribution['recurring'])) {
    $xml .=	  '<RecurringContrib>'.$contribution['recurring'].'</RecurringContrib>';
    $xml .=	  '<RecurringContribNote />';
  }
  else {
    $xml .=	  '<RecurringContrib /><RecurringContribNote />';
  }
  $xml .=     '<Amount>'.htmlspecialchars($contribution['amount']).'</Amount>';
  $xml .=     !empty($contribution['notes']) ? '<Attend>'.htmlspecialchars($contribution['notes']).'</Attend>' : '<Attend />';
  if (!empty($contribution['recurring_enable']) && ($contribution['recurring_enable'] == 'true') && !empty($contribution['recurring'])) {
    $xml .=	  !empty($contribution['recurring_period']) ? '<RecurringPeriod>'.$contribution['recurring_period'].'</RecurringPeriod>' : '<RecurringPeriod>MONT</RecurringPeriod>';
    $xml .=	  '<RecurringTerm>'.$contribution['recurring_term'].'</RecurringTerm>';
  }
  $xml .=     !empty($contribution['keycode']) ? '<KeyCode>BCE_' . htmlspecialchars($contribution['keycode']) . '</KeyCode>' : '';
  $xml .=   '</ContributionInfo>';
  $xml .=   '<VerisignInfo>';
  $xml .=     '<CreditCardNumber>'.$contribution['credit_card_number'].'</CreditCardNumber>';
  $xml .=     '<ExpYear>'.$contribution['exp_year'].'</ExpYear>';
  $xml .=     '<ExpMonth>'.$contribution['exp_month'].'</ExpMonth>';
  $xml .=     !empty($contribution['cvv']) ? '<CVV>'.$contribution['cvv'].'</CVV>' : '<CVV />';
  $xml .=   '</VerisignInfo>';
  $xml .= '</PostVerisignTransaction>';

  $response = _crmngp_cwp_request($xml,'PostVerisignTransaction', NULL, $contribution['sendemail']);
  $simple = $response['PostVerisignTransactionResult'];
  $p = xml_parser_create();
  xml_parse_into_struct($p, $simple, $vals, $index);

  if (empty($vals[2])) {
    return array('code' => -666, 'response' => array('No response received from server'), 'values' => $vals);
  }
  else if ($vals[2]['value'] != 'Success') {
    return array('code' => -666, 'response' => $response, 'values' => $vals);
  }
  else {
    return array('code' => $vals[6]['value'], 'message' => $vals[7]['value'], 'referencenumber' => $vals[8]['value']);
  }
}

/**
 * Adds a contact to NGP through the NGPAPI. If a contact_id
 * is passed, the contact will be updated instead of added.
 *
 * @param $contact
 *   An array of key values that represents this contact.
 * @return $contact_id
 *   The numerical ID of this contact, new or existing.
 */
function crmngp_cwp_volunteer_save($contact = NULL) {
  if (!$contact) { return NULL; }

  $contact['phone_home'] = preg_replace('/\D/','',$contact['phone_home']);
  $contact['phone_work'] = preg_replace('/\D/','',$contact['phone_work']);

  $xml = '<VolunteerSignUp xmlns="http://ngpsoftware.com/NGP.Services.UI/OnlineContribService">';
  $xml .=   '<Credentials>'.variable_get('crmngp_cwp_credentials', NULL).'</Credentials>';
  $xml .=   _crmngp_cwp_volunteer_xml(_crmngp_cwp_contact_field_transform($contact));
  $xml .= '</VolunteerSignUp>';

  $response = _crmngp_cwp_request($xml, 'VolunteerSignUp', 'volunteer'); // return only the contact_id.
  return $response['processRequestWithCredsResult']['contact'.$withcodes.'SetOutput']['contact']['contactID'];
}

/**
 * Generates the XML to represent a contact within the NGPAPI.
 *
 * @param $contact
 *   An array of valid fields for the NGPAPI in the order the
 *   API expects (as returned from _crmngp_crm_contact_field_transform).
 *   If no $contact passed, we return an empty <contact> element.
 * @return $xml
 *   The XML that represents a contact in the NGPAPI.
 */
function _crmngp_cwp_volunteer_xml($contact = NULL) {
  $xml .= '<ContactInfo>';
  foreach ($contact as $element => $value) {
    if ($element != 'codes') {
      $xml .= "<$element>$value</$element>";
    }
  }
  $xml .= '</ContactInfo>';
  if (is_array($contact['codes'])) {
    foreach ($contact['codes'] as $code) {
      $xml .= '<VolunteerInfo><Code>'.$code.'</Code></VolunteerInfo>';
    }
  }
  return $xml;
}

/**
 * Sends the XML to the remote SOAP server and returns the response.
 *
 * @param $xml
 *   The XML request that will be sent inside the processRequest.
 * @param $type
 *   The type of transaction that the passed $xml represents.
 * @return $response
 *   The response from the SOAP request.
 */
function _crmngp_cwp_request($xml = NULL, $type = NULL, $wsdl = NULL, $sendemail = FALSE) {
  if (!$xml || !$type) { return NULL; } // validation of incoming.
  if (!in_array($type, _crmngp_cwp_transaction_types())) { return NULL; }
  if (!variable_get('crmngp_cwp_credentials', NULL)) { return NULL; }

  $params = array(
    'credentials' => variable_get('crmngp_cwp_credentials', NULL),
    'data' => $xml,
    'sendEmail' => ($sendemail == 'True') ? TRUE : FALSE,
  );

  $client = new nusoap_client(variable_get('crmngp_cwp_wsdlurl', NULL), "wsdl", NULL, NULL, NULL, NULL, 60, 90);
  //$client->decode_utf8 = false;
  $result = $client->call($type, $params);

  if (empty($result)) {
    $result = array('debug' => $client->debug_str);
  }

  if ((variable_get('crmngp_debug', 0)) && (user_access('administer site configuration'))) { // Display debug info to admins if option in settings is checked.
    $debug_request = $client->request;
    $debug_response = $client->response;
    $debug_extended = $client->debug_str;

    $debug  = '<h2>Request</h2><code>' . htmlspecialchars($debug_request, ENT_QUOTES) . '</code>';
    $debug .= '<h2>Response</h2><code>' . htmlspecialchars($debug_response, ENT_QUOTES) . '</code>';

    drupal_add_js('misc/drupal.js');
    drupal_add_js('misc/collapse.js');

    $extended_debug = '<pre>' . htmlspecialchars($debug_extended, ENT_QUOTES) . '</pre>';
    $fieldset = array(
      '#title' => "Extended debug output",
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $extended_debug
    );
    $debug .= theme('fieldset', $fieldset);

    $result_array = '<pre>' . print_r($result, 1) . '</pre>';
    $fieldset = array(
      '#title' => "Result array",
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $result_array
    );
    $debug .= theme('fieldset', $fieldset);

    $debug .= '<hr />'; // Separate individual requests.

    $fieldset = array(
      '#title' => "$type debug output",
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#value' => $debug
    );
    drupal_set_message(theme('fieldset', $fieldset));
  }

  return $result;
}

/**
 * Transform to or from standardized field names or NGP field names.
 *
 * Under normal circumstances, we'd have made sure that our form sends the
 * right keys in the first place, but we're looking towards the future for a
 * standardized API to various CRMs, and this transformation represents those
 * "standard" key names turning into "CRM-specific" key names, and vice-versa.
 * This also serves as an indication of what data this CRM can accept, as
 * submitted keys that are not part of the transformation aren't passed along.
 *
 * For NGPAPI, the ORDER of the fields in the submitted SOAP request are
 * also important and if we're transforming TO NGP field names, the newly
 * returned array from this function will represent the proper order.
 *
 * @param $submitted_contact
 *   An array of submitted fields representing a contact.
 * @param $direction
 *   One of "to_standard" or "to_crm", defaults to 'to_crm'.
 * @return $modified_contact
 *   An array of transformed fields per $direction.
 */
function _crmngp_cwp_contact_field_transform($submitted_contact = NULL, $direction = 'to_crm') {
  if (!$submitted_contact) { return NULL; }

  $transforms = array(
    'contact_id'     => 'ContactID',
		'first_name'     => 'FirstName',
		'last_name'      => 'LastName',
		'middle_name'    => 'MiddleName',
		'prefix'         => 'Prefix',
		'suffix'         => 'Suffix',
		'address1'       => 'Address1',
		'address2'       => 'Address2',
		'address3'       => 'Address3',
		'city'           => 'City',
		'state'          => 'State',
		'zip'            => 'Zip',
		'greeting'       => 'Salutation',
		'email'          => 'Email',
		'phone_home'     => 'HomePhone',
		'phone_work'     => 'WorkPhone',
		'phone_work_ext' => 'WorkExtension',
		'phone_fax'      => 'FaxPhone',
		'occupation'     => 'Occupation',
		'employer'       => 'Employer',
		'contact_type'   => 'contactType',
		//'optin'          => 'smsOptIn',
		'place_of_business' => 'placeOfBusiness',
		'codes'          => 'codes',
		'amount'		     => 'Amount',
		'credit_card_number' => 'CreditCardNumber',
		'exp_year'		   => 'ExpYear',
		'exp_month'		   => 'ExpMonth',
		'cvv'	      	   => 'CVV',
  );

  if ($direction == 'to_standard') {
    $transforms = array_flip($transforms);
  }
  $modified_contact = array();
  foreach ($transforms as $old => $new) {
    if ($submitted_contact[$old]) {
      $modified_contact[$new] =
        $submitted_contact[$old];
    }
  }

  return $modified_contact;
}

/**
 * @return $fields
 *   An array of field name and human-intended labels. This list,
 *   being intended for user consumption, does not include the
 *   following NGPAPI fields: contact_id, type, committee_id.
 */
function crmngp_cwp_contact_field_labels($type = 'contactadd') {
  return array(
    'first_name'     => 'First Name',
    'last_name'      => 'Last Name',
    'middle_name'    => 'Middle Name',
    'prefix'         => 'Prefix (Mr., Mrs., etc.)',
    'suffix'         => 'Suffix (Jr., III, etc.)',
    'mail_name'      => 'Mail Name',
    'address1'       => 'Address 1',
    'address2'       => 'Address 2',
    'address3'       => 'Address 3',
    'city'           => 'City',
    'state'          => 'State',
    'zip'            => 'Zip',
    'greeting'       => 'Greeting',
    'email'          => 'Email',
    'phone_home'     => 'Phone (Home)',
    'phone_work'     => 'Phone (Work)',
    'phone_work_ext' => 'Phone (Work Extension)',
    'phone_fax'      => 'Phone (Fax)',
    'occupation'     => 'Occupation',
    'employer'       => 'Employer',
    'codes'          => t(variable_get('crmngp_'.$type.'_codes_label', 'Codes')),
  );
}

/**
 * @return $types
 *   A list of valid transaction types used in the NGP CWP API.
 */
function _crmngp_cwp_transaction_types() {
  return array(
    'EmailSignUp',
    'VolunteerSignUp',
    'PostVerisignTransaction',
  );
}
