

        CRMNGP MODULE - README
______________________________________________________________________________

NAME:       NGP Campaign Office Online� / Contribution Web Package� Integration
AUTHOR:     Sean Robertson <seanr@ngpsoftware.com>

For additional questions, please contact us:
p: 202.686.9330 � f: 202.686.9331 � e: sales@ngpsoftware.com
______________________________________________________________________________


DESCRIPTION


This module provides Drupal integration with the NGP Software API enabling you 
to quickly integrate your web site with NGP Campaign Office� and Contribution Web 
Package� for efficient campaign management.  

NGP Campaign Office� is the leading fundraising and compliance software for 
Democrats and their allies.  Campaign Office� offers easy and effective systems 
for fundraising, donor management, data management, compliance reporting, 
broadcast email, and integration with other campaign systems. NGP also offers 
highly regarded and unlimited customer support and customized training, as well 
as targeting, appending, and data hygiene services. 


This module provides you configuration tools to easily create NGP integrated
registration, email subscription, volunteer collection and online contribution
forms.

To become an NGP client or to upgrade your NGP Campaign Office� account with the
Contribution Web Package� please contact us at sales@ngpsoftware.com


INSTALLATION

Step 1)
  Download latest release from http://drupal.org/project/crmngp
  
  Make sure you have also installed the latest release of crmapi module from
  http://drupal.org/project/crmapi

  This module requires NuSOAP. You can download NuSOAP here:
  http://sourceforge.net/project/showfiles.php?group_id=57663
  
  Extract the zip file into the sites/all/libraries directory (you may need to 
  create it if it doesn't already exist). Alternatively, if you have shell access
  you can run this command from within the modules/crmngp directory:
  
  cvs -d:pserver:anonymous@nusoap.cvs.sourceforge.net:/cvsroot/nusoap co -P lib

Step 2)
  Extract the package into your 'modules' directory.


Step 3)
  Enable the module.

  Go to "administer -> build -> modules" and put a checkmark in the 'status' 
  column next to 'NGP COO/CWP', then click Save Configuration.


Step 4)
  Go to "administer -> site configuration -> NGP COO/CWP" to configure the 
  module.

  Make sure the domain listed in the first field is correct for your site.

  Log into Campaign Office as the user you want to use for API transactions (it 
  is often useful to create a dedicated API user for this).  Go to "File -> 
  Preferences -> API Preferences" and copy the CampaignID, UserID, and 
  Credentials String into the matching fields on the module's configuration 
  page.

  In Campaign Office, go to "File -> Preferences -> CWP Preferences", scroll to 
  the bottom of the page and enter your login information in the CWP API 
  Credentials form, click Generate, and copy the string it generates to the 
  module's configuration page.


Step 5)
  Go to "administer ->> site configuration -> CRM API" then click the Fields tab 
  to configure the fields for each form.  The default values should be good for 
  most sites, but you can add or remove fields from each form as well as 
  determine which should be required.  Note: some fields are required by COO or 
  CWP for each form and cannot be disabled.


Step 6)
  Enable permissions appropriate to your site.

  The crmngp module provides one permission: - 'administer NGP API settings': 
  allow users to configure crmngp.


IMPORTANT NOTE

crmngp cache's the CWP preferences to avoid hitting the API excessively.  To 
reset that cache manually, just visit crmngp's config page at Administer -> 
Settings -> NGP COO/CWP API.  That automatically refreshes the varaibles with 
the latest data from the API.  It will also update automatically any time cron 
is run (see the Drupal handbook for information on setting up cron).


SUPPORT

All issues with this module should be reported via the following form:
http://drupal.org/node/add/project_issue/crmngp

______________________________________________________________________________
http://www.ngpsoftware.com