// $Id: maine.js,v 1.1 2010/01/22 22:36:08 seanr Exp $

$(document).ready(function(){
  var evt = $.browser.msie ? "click" : "change";
  
  if ($.browser.msie) {
    var expand   = function(){ $(this).width(120) }
    var contract = function(){ if (!this.noHide) $(this).width(60) }
    var focus    = function(){ this.noHide = true }
    var blur     = function(){ this.noHide = false; contract.call(this) }
    $("#edit-state")
      .hover(expand, contract)
      .focus(focus)
      .click(focus)
      .blur(blur)
      .change(blur)
  }
  
  if ($("select[name='ky_status']").val() != '2') {
    $("div#crmngp-spouse").hide();
  }
  
  $("select[name='ky_status']").bind(evt, function(){
    //alert($(this).val());
    if ($(this).val() == '2') {
      $("div#crmngp-spouse").show();
    }
    else {
      $("div#crmngp-spouse").hide();
    }
  });
});