
$(document).ready(function(){
  var evt = $.browser.msie ? "click" : "change";
  
  if ($.browser.msie) {
    var expand   = function(){ $(this).width(120) }
    var contract = function(){ if (!this.noHide) $(this).width(60) }
    var focus    = function(){ this.noHide = true }
    var blur     = function(){ this.noHide = false; contract.call(this) }
    $("#edit-state")
      .hover(expand, contract)
      .focus(focus)
      .click(focus)
      .blur(blur)
      .change(blur)
  }
  
  if ($("#edit-resident").attr('checked') == false) {
    $("#edit-resident-address1").attr("disabled","disabled");
    $("#edit-resident-city").attr("disabled","disabled");
    $("#edit-resident-zip").attr("disabled","disabled");
  }
  
  $("#edit-resident").bind(evt, function(){
    if ($(this).attr('checked')) {
      $("#edit-resident-address1").removeAttr("disabled");
      $("#edit-resident-city").removeAttr("disabled");
      $("#edit-resident-zip").removeAttr("disabled");
    }
    else {
      $("#edit-resident-address1").attr("disabled","disabled");
      $("#edit-resident-city").attr("disabled","disabled");
      $("#edit-resident-zip").attr("disabled","disabled");
    }
  });
});